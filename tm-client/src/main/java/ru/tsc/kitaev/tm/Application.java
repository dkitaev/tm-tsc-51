package ru.tsc.kitaev.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.component.Bootstrap;

public class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
